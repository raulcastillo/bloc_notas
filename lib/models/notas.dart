import 'dart:convert';

// Nota notaFromJson(String str) => Nota.fromJson(json.decode(str));

// String notaToJson(Nota data) => json.encode(data.toJson());

class Nota {
  int? id;
  String? titulo;
  String? descripcion;
  Nota({
    this.id,
    this.titulo,
    this.descripcion,
  });

  // factory Nota.fromJson(Map<String, dynamic> json) {
  //   return Nota(
  //     clave: json["ART_CLAVE"],
  //     nombre: json["ART_NOMBRE"],
  //     descripcion: json["ART_DESCRIPCION"],
  //   );
  // }

  // Map<String, dynamic> toJson() {
  //   return {
  //     "ART_CLAVE": clave,
  //     "ART_NOMBRE": nombre,
  //     "ART_DESCRIPCION": descripcion,
  //     "ART_MODELO": modelo,
  //     "active": active,
  //     "ART_SUCURSAL_TASA_CERO": tasaCero,
  //     "marca": marca,
  //     "precio_base": precio,
  //     "existencia": existencia,
  //     "unidad_medida": unidadMedida,
  //   };
  // }

  Nota.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    titulo = map["titulo"];
    descripcion = map["descripcion"];
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "titulo": titulo,
      "descripcion": descripcion,
    };
  }
}

// List<Articulo> articulosFromJson(String strJson) {
//   final str = json.decode(strJson);
//   return List<Articulo>.from(str.map((item) {
//     return Articulo.fromJson(item);
//   }));
// }

// String articulosToJson(Articulo data) {
//   final dyn = data.toJson();
//   return json.encode(dyn);
// }
