// import 'package:crud/models/note.dart';

import 'dart:io';

import 'package:bloc_notas/models/notas.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class TaskDatabase {
  Database? _database;
  TaskDatabase._();
  static final TaskDatabase db = TaskDatabase._();
  // initDB()async{
  //   _db = await openDatabase('my_db.db', version: 1,
  //   onCreate: (Database db, int version){

  //   );
  //   print("DB INITIALIZED");
  // }

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await getDatabaseInstanace();
    return _database;
  }

  Future<Database?> getDatabaseInstanace() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, 'my_db.db');

    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
          "CREATE TABLE articulos (ART_ID INTEGER PRIMARY KEY, ART_CLAVE TEXT, ART_NOMBRE TEXT, ART_DESCRIPCION TEXT, ART_MODELO TEXT, active INTEGER, ART_SUCURSAL_TASA_CERO INTEGER, marca TEXT, precio_base TEXT, existencia INTEGER, unidad_medida TEXT)");
      await db.execute(
          "CREATE TABLE carrito (ART_ID INTEGER PRIMARY KEY, ART_CLAVE TEXT, ART_NOMBRE TEXT, ART_DESCRIPCION TEXT, ART_MODELO TEXT, active INTEGER, ART_SUCURSAL_TASA_CERO INTEGER, marca TEXT, precio_base TEXT, existencia INTEGER, unidad_medida TEXT)");
    });
  }

  Future<void> insert(Nota nota, String tabla) async {
    Database? db = await database;

    db!.insert("$tabla", nota.toMap());
  }

  Future<List<Nota>> getAllTasks() async {
    Database? db = await database;
    List<Map<String, dynamic>> results = await db!.query("articulos");
    print(results);
    return results.map((map) => Nota.fromMap(map)).toList();
  }

  Future<List<Nota>> getAllCarrito() async {
    Database? db = await database;
    List<Map<String, dynamic>> results = await db!.query("carrito");
    print(results);
    return results.map((map) => Nota.fromMap(map)).toList();
  }

  // Future<List<Nota>> getSearch(Nota articulo) async {
  //   Database? db = await database;
  //   List<Map<String, dynamic>> results = await db!.query("articulos",
  //       where: "ART_CLAVE = ?", whereArgs: [articulo.clave]);
  //   print(results);
  //   return results.map((map) => Articulo.fromMap(map)).toList();
  // }

  //   Future<void> getExistencia(Articulo articulo) async {
  //   Database? db = await database;
  //   List<Map<String, dynamic>> results = await db!.query("carrito",
  //       where: "ART_CLAVE = ?", whereArgs: [articulo.clave]);
  //   print(results);
  //   results.map((map) => Articulo.fromMap(map)).toList();
  //   if(results.length>0){
  //     return
  //   }
  // }

  deleteAll(String tabla) async {
    final db = await database;
    db!.delete("$tabla");
  }

  deleteOne(String tabla, String clave) async {
    final db = await database;
    db!.delete("$tabla", where: "ART_CLAVE = ?", whereArgs: [clave]);
  }
}
// class Operation {
//   static Future<Database> _openDB() async {
//     return openDatabase(join(await getDatabasesPath(), 'continental.db'),
//         onCreate: (db, version) {
//       return db.execute(
//         "CREATE TABLE articulos (id INTEGER PRIMARY KEY, clave TEXT, nombre TEXT, descripcion TEXT, modelo TEXT, active INTEGER, marca TEXT, precio TEXT, existencia INTEGER, sublinea TEXT, unidadMedida TEXT)",
//       );
//     }, version: 1);
//   }

//   static Future<void> insert(Articulo articulo) async {
//     Database database = await _openDB();

//     return database.insert("articulos", articulo.toMap());
//   }

//   static Future<void> delete(Note note) async {
//     Database database = await _openDB();

//     return database.delete("notes", where: 'id = ?', whereArgs: [note.id]);
//   }

//   static Future<void> update(Note note) async {
//     Database database = await _openDB();

//     return database.update("notes",note.toMap(), where: 'id = ?', whereArgs: [note.id]);
//   }

//   static Future<List<Note>> notes() async {
//     Database database = await _openDB();

//     final List<Map<String, dynamic>> notesMap = await database.query("notes");

//     for (var n in notesMap) {
//       print("____" + n['title']);
//     }

//     return List.generate(
//         notesMap.length,
//         (i) => Note(
//             id: notesMap[i]['id'],
//             title: notesMap[i]['title'],
//             content: notesMap[i]['content']));
//   }
// }
